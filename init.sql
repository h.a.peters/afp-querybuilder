drop database if exists test_db;

create database test_db;
grant all privileges on database "test_db" to postgres;