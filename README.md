# DBasic

## Summary

DBasic is Object Relation Mapping (ORM) library that allows you to specify your models and relations as haskell datatypes. 
The backend code that runs migrations on the database and builds the final SQL queries are automatically generated using Template Haskell.

### DBHS

The models are defined in a special .dbhs file whose syntax is very
similar to Haskell record type syntax, with some added relationship
keywords. An example database specification is as follows:

```haskell
Model Blog = 
 { blogId :: Primary
 , author :: String (50) 
 , comments :: ManyRef Comment
 }

Model Comment = 
  { commentId :: Primary 
  , title :: String (100) 
  , content :: String (255)
  }
```

Every model defines a Haskell data type and corresponding SQL table. A
model can contain Int, Strings, Booleans and other models. Our library
will generate the SQL migration, Haskell data types, class instances
and everything else that you'll need to create, use and query these
data types in a type safe, Haskell like way. In your own project,
simply create the following `Models.hs` file:

```haskell
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}


module Models where

import Database.DBasic.Queries
import Database.DBasic.Models

$(generateModels "./models.dbhs")
```

The final line will be subsituted with the generated code which contains, but is not excluded to, the following haskell record types: 

```haskell
data Blog = 
 { blogId :: Int
 , author :: String 
 , comments :: Maybe [Comment]
 }

data Comment = 
  { commentId :: Int 
  , title :: String
  , content :: String
  }
```

### Querying

#### Legend

DBasic uses some modified version of `$`, `<$>`, and `>>=` with different argument orders and precedence to be able to chain operations more easily.

```haskell
x |> f = f x
x |>> f = x <$> f
x |>= f = x >>= f
```

#### SELECT queries

DBasic allows you to compose a variety of operations like `whereClause`, `skip`, `limit`. Finally a `QueryBuilder a` instance can be given to the `fetchOneOrNothing` or `fetchMany` function that compiles and executes the query and returns the resulting `a` or `[a]` in a `DBIO` monad, which can be evaluated to its `IO` equivalent given a `DbConn` instance.

A number of example queries are given below.

Simplest query, collect the entire table.
```haskell
allComments :: DBIO [Comment]
allComments = query @Comment
                 |> fetchAll
```

Basic where condition.

```haskell
hugosComments :: QueryBuilder Comment
hugosComments = query @Comment
                  |> whereClause #author Eq "Hugo"
```

Or conditions

```haskell
hugosAndNielsComments :: QueryBuilder Comment
hugosAndNielsComments = query @Comment
                  |> queryOr [ whereClause #author Eq "Hugo"
                             , whereClause #author Eq "Niels
                             ]
```

And conditions

```haskell
hugosTODOComments :: QueryBuilder Comment
hugosTODOComments = query @Comment
                  |> queryAnd [ whereClause #author Eq "Hugo"
                              , whereClause #content Eq "TODO"
                              ]
```

Which can be more simply expressed as repeated where clauses

```haskell
hugosAndNielsComments :: QueryBuilder Comment
hugosAndNielsComments = query @Comment
                           |> whereClause #author Eq "Hugo"
                           |> whereClause #author Eq "Niels
```

Restricting the size of a list query

```haskell
fetchTenComments :: Int -> QueryBuilder Comment
fetchTenComments offset = query @Comment
                         |> skip offset
                         |> limit 10
```

Fetching related values, this happens on the value level outside the `QueryBuilder`

```haskell
fetchBlogWithComments :: DBIO (Maybe Blog)
fetchBlogWithComments = query @Blog
                          |> fetchOneOrNothing
                          |> fetchManyRelatedMaybe @Comment
```

#### INSERT queries

We can insert new items into the database by creating new records:

```haskell
createBlog :: DBIO Blog
createBlog = newRecord @Blog
           |> set #author "Stallman"
           |> set #comments [
               newRecord @Comment
                 |> set title "Amazing blogpost"
                 |> set content "BTW, it is GNU+Linux, not Linux.",
               newRecord @Comment
                 |> set title "Is this blog open source"
                 |> set content "I don't believe in the GPL"]
           |> createRecord
```

This inserts a `Blog` and two `Comment`s into the database.

#### UPDATE queries

We can update items, by selecting them, changing them and then
updating them.

```haskell
updateLol :: DBIO Lol
updateLol = hugoComments
     |>> modifyM #author "Not Hugo"
     |>= updateRecord

```

#### DELETE queries

We can delete items by selecting them and then running
`deleteRecord`. All children of the record will be deleted as well.

```haskell
deleteBlog :: DBIO ()
deleteBlog = do
  blogs <- query @Blog
            |> whereClause #author Eq "Marien"
            |> fetchAll
  mapM_ deleteRecord blogs
```

### Obtaining a DbConn

All DBasic needs to know is the connection string of the underlying postgres database. An instance of the `Config` can created as follows:

```haskell
config :: Config
config = Config {
      port = 5432
    , dbName = "test_db"
    , dbUser = "postgres"
    , dbPassword = "changeme"
    , dbHost = "localhost"
  }
```

As part of the generated code, the function `mkDbConn` is exposed. It can be given the config to return an `IO DbConn`:

```haskell
connection :: IO DbConn
connection = mkDbConn config
```

### Executing queries

Using the `allComments` function from above, we can perform the query using our connection.

```haskell
main :: IO ()
main = do
    conn <- connection
    comments <- runDBIO conn (fetchAll allComments)
    mapM_ print (get #content)
```

