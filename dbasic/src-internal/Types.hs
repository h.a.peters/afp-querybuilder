{-# LANGUAGE ImplicitParams #-}
{-# LANGUAGE RankNTypes #-}
module Types where

import qualified Data.ByteString.UTF8 as B
import qualified Database.PostgreSQL.Simple as PG

-- | The 'DBIO' type wraps the IO monad in an environment
-- where a database connection ('DbConn') has to be present.
-- This is achieved using an implicit parameter. Note that
-- any function that returns a 'DBIO' is able to use this parameter.
type DBIO a = (?conn :: DbConn) => IO a

-- | the 'Config' type holds all information for the database connection.
-- an instance for this config is passed to 'mkDbConn' function that is generated
-- by the library.
data Config = Config
    {   port :: Int
      , dbName :: String
      , dbUser :: String
      , dbPassword :: String
      , dbHost :: String
    }

-- function that converts a 'Config' to a 'Bytestring' that is used to connect
-- to the database.
getConfigStr :: Config -> B.ByteString
getConfigStr config = B.fromString $ "port= " <> "'" <> show (port config) <> "'" 
                    <> " dbname= " <> "'" <> dbName config <> "'"
                    <> " user= " <> "'" <> dbUser config <> "'"
                    <> " password= " <> "'" <> dbPassword config <> "'"
                    <> " host= " <> "'" <> dbHost config <> "'" 

-- | 'DbConn' holds the connection to the database facilitated by
-- Database.Postgres.Simple.
newtype DbConn = DbConn PG.Connection

-- | 'MigrateSQL' holds the migration statements.
-- For now this contains the CREATE IF EXISTS statements for all models
-- in the .dbhs file.
newtype MigrateSQL = MigrateSQL String 
    deriving (Show)
