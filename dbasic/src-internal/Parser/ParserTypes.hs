module Parser.ParserTypes where

import Data.Char ( toLower )

type ModelName = String

-- A model constists of a tableName, a list of fields and
-- a list of other models that it might be referenced by.
data Model = Model {
  tableName :: ModelName,
  fields :: [Field],
  referencedBy :: [(ModelName, Field)]
} deriving (Show)

-- Every field consists of a name and a type.
data Field = Field String FieldType
  deriving (Show)

-- A type might be nullable, a primary key or a basic type.
-- A field may also reference another model in a many to one 
-- or one to one relation.
data FieldType
  = Nullable BaseType
  | PrimKey
  | Basic BaseType
  | RefMany String
  | RefOne String
  deriving (Show)

-- The Base types.
data BaseType
  = Integer
  | Boolean
  | VarChar Int
  deriving (Show)

-- The tokens that the lexer uses to lex the .dbhs file.
data Token = TModel | TModelName String | TModelStart | TFieldName String
           | TFieldTypeSep | TPrimType | TNullable | TRefMany | TRefOne
           | TFieldType String | TFieldSep | TModelRef String
           | TFieldLength Int | TModelEnd deriving (Show, Eq)

-- Gets the field name.
fieldName :: Field -> String
fieldName (Field n _) = n

-- Gets the field type.
fieldType :: Field -> FieldType
fieldType (Field _ t) = t

-- checks if a field is a primkey.
isPrimKey :: Field -> Bool
isPrimKey (Field _ PrimKey)     = True
isPrimKey _                     = False

-- Gets the name of a foreign key for code generation.
foreignKeyName :: String -> String
foreignKeyName n = "__" ++ map toLower n ++ "_id"

-- Gets the primkey of a model.
getPrimKey :: Model -> Field
getPrimKey model = head $ filter isPrimKey (fields model)



