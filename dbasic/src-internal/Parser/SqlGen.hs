module Parser.SqlGen where

import Parser.ParserTypes
    ( foreignKeyName,
      BaseType(..),
      Field(..),
      FieldType(..),
      Model(tableName, fields, referencedBy) )

import Data.List
    ( (++), filter, map, elem, concatMap, intercalate, sortBy )

import Prelude hiding (Integer)

generateSQL :: [Model] -> String
generateSQL models = concatMap generateCreateForModel $
                     sortByDependencies models

sortByDependencies :: [Model] -> [Model]
sortByDependencies = sortBy dependsOn
  where
    -- Determine order
    m1 `dependsOn` m2
      | tableName m1 `elem` map fst (referencedBy m2) = LT
      | tableName m2 `elem` map fst (referencedBy m1) = GT
      | otherwise                                     = EQ

generateCreateForModel :: Model -> String
generateCreateForModel model
  = "CREATE TABLE IF NOT EXISTS " <> tableName model <> "\n"
    <> "(\n\t"
    <> createFields model
    <> "\n);\n"

createFields :: Model -> String
createFields model = intercalate ",\n\t"
                     $ realFields ++ foreignFields
  where
    realFields = map createField $ filter isRealField $ fields model
    foreignFields = map createForeignField $ referencedBy model

isRealField :: Field -> Bool
isRealField (Field _ (RefMany _)) = False
isRealField (Field _ (RefOne _))  = False
isRealField _                     = True

createField :: Field -> String
createField (Field name t) = name <> " " <> toSqlType t

toSqlType :: FieldType -> String
toSqlType (Nullable b) = baseTypeToSqlType b
toSqlType PrimKey      = "SERIAL PRIMARY KEY"
toSqlType (Basic b)    = baseTypeToSqlType b <> " NOT NULL"

toSqlBasicType :: FieldType -> String
toSqlBasicType = baseTypeToSqlType . getBaseType

baseTypeToSqlType :: BaseType -> String
baseTypeToSqlType Integer     = "INTEGER"
baseTypeToSqlType Boolean     = "BOOLEAN"
baseTypeToSqlType (VarChar l) = "VARCHAR(" <> show l <> ")"

getBaseType :: FieldType -> BaseType
getBaseType (Nullable b) = b
getBaseType (Basic b)    = b
getBaseType PrimKey      = Integer

createForeignField :: (String, Field) -> String
createForeignField (reference, Field n t)
  = foreignKeyName reference <> " " <> toSqlBasicType t
    <> " REFERENCES " <> reference <> " (" <> n <> ")"
