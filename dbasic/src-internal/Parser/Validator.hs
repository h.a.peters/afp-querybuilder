module Parser.Validator (validateModel) where

import Parser.ParserTypes
    ( BaseType,
      Field(..),
      FieldType(RefOne, PrimKey, Nullable, Basic, RefMany),
      Model(Model) )
import Data.List (intercalate)
import Data.Maybe ( mapMaybe )

-- Algebra for the Model data type.
type ModelAlgebra ms m f ft = ( [m]    -> ms                     -- [Model]
                              , String -> [f]
                                -> [(String, Field)] -> m        -- Model
                              , String -> ft  -> f               -- Field
                           
                              , ft                               -- PrimKey
                              , BaseType -> ft                   -- Nullable
                              , BaseType -> ft                   -- Basic
                              , String   -> ft                   -- RefMany
                              , String   -> ft                   -- RefOne
                           )
-- Fold function
foldModel :: ModelAlgebra ms m f ft -> [Model] -> ms
foldModel ( fModels
          , fModel
          , fField
          , fPrimKey
          , fNullable
          , fBasic
          , fRefMany
          , fRefOne
          ) = fms
          where fms ms               = fModels (map fm ms)
                fm  (Model tn fs rs) = fModel tn (map ff fs) rs
                ff  (Field fn ft)    = fField fn (fft ft)
                fft PrimKey          = fPrimKey
                fft (Nullable bt)    = fNullable bt
                fft (Basic bt)       = fBasic bt
                fft (RefMany xs)     = fRefMany xs
                fft (RefOne xs)      = fRefOne xs

-- Error message type
type ErrorMsg = String

-- Main validation function.
validateModel :: [Model] -> Maybe ErrorMsg
validateModel models = case allChecks of
                        [] -> Nothing
                        xs -> Just $ buildErrorMsg xs
  where allChecks = valOnePrimKey models ++ valRef models

-- Concatenates all resulting error messages in a concise error message.
buildErrorMsg :: [ErrorMsg] -> ErrorMsg
buildErrorMsg xs = "During Parser Validation, the following errors occured: \n"
                   ++ intercalate "\n" xs

-- Validate that model only has one primary key.
valOnePrimKey :: [Model] -> [ErrorMsg]
valOnePrimKey models = concatMap validate (foldRes models)
  where validate pks | pks > 0 && pks < 2 = []
                     | otherwise = ["Validation error! All Models should have exactly one Primary Key"]
        foldRes = foldModel valOnePrimKeyAlg

-- Fold functions for checking if a model only has one primary key.
valOnePrimKeyAlg :: ModelAlgebra [Int] Int Int Int
valOnePrimKeyAlg = ( id
                   , \_ is _ -> sum is
                   , \_ i  -> i
                   , 1, const 0, const 0
                   , const 0, const 0
                   )
-- Checks if referenced models actually exist.
valRef :: [Model] -> [ErrorMsg]
valRef models = case refCheck of
                  Nothing -> []
                  Just xs -> ["Validation error! the following models have non existing references: "
                              ++ intercalate ", " xs]
  -- First all model names are obtained using another algebra.
  where refCheck = foldModel (valRefAlg (obtainModelNames models)) models

-- Fold functions to validate that Reffed model exists.
valRefAlg :: [String] -> ModelAlgebra (Maybe [String]) ([String] -> Maybe String) 
                          ([String] -> Bool) ([String] -> Bool)
valRefAlg mNames = ( (`valRefModels` mNames)
                   , valRefModel
                   , \n fts env -> fts env
                   , const True, \_ _ -> True, \_ _ -> True
                   , elem, elem)

-- Function used by 'valRefAlg'.
valRefModels :: [[String] -> Maybe String] -> [String] -> Maybe [String]
valRefModels fs xs = case mapMaybe (\f -> f xs) fs of
                      [] -> Nothing
                      xs -> Just xs

valRefModel :: String -> [[String] -> Bool] -> [(String, Field)] -> [String] -> Maybe String
valRefModel n fs _ env = if all (\f -> f env) fs then Nothing else Just n

-- Obtains all model names.
obtainModelNames :: [Model] -> [String]
obtainModelNames = foldModel obtainModelNamesAlg

obtainModelNamesAlg :: ModelAlgebra [String] String () ()
obtainModelNamesAlg = ( id, \i _ _ -> i, \_ _-> ()
                      , (), const (), const()
                      , const (), const ()
                      )
