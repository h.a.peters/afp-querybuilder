{-# LANGUAGE FlexibleContexts #-}
{-# OPTIONS_GHC -Wno-unused-do-bind #-}
{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
module Parser.Parser (runDBParser, parseFromFile) where

import Parser.ParserTypes
    ( Field(..), FieldType(..), Model(..), Token(..),
      BaseType (Integer, VarChar, Boolean), fieldType,
      fieldName, getPrimKey )
import Parser.Lexer ( runLexer )
import Text.Parsec
    ( ParseError,
      sepBy,
      (<|>),
      many,
      parse,
      tokenPrim,
      Parsec(..),
      incSourceColumn,
      incSourceLine,
      setSourceColumn )
import Text.Parsec.Prim
    ( Stream, ParsecT, many, parse, tokenPrim, unexpected )
import Text.Parsec.Pos
    ( incSourceColumn, incSourceLine, setSourceColumn )

type TokenParser a = Parsec [Token] () a

-- |Adds info to models that are referenced by other models.
--
-- In the decleration it is only declared in a model that it
-- references another model. But for the code and SQL generation it is
-- useful to also know what models reference this model.
addReferenceInfo :: [Model] -> [Model]
addReferenceInfo models = map addReferenceInfoTo models
  where addReferenceInfoTo model = model {
          referencedBy = [(tableName m, getPrimKey m)
                         | m <- models, references model m]
          }
        references m referencedBy = any (fieldReferences m . fieldType)
                                    $ fields referencedBy
        fieldReferences m (RefMany s) = tableName m == s
        fieldReferences m (RefOne s)  = tableName m == s
        fieldReferences _ _           = False


-- |Parses the models from a file
parseFromFile :: FilePath -> IO [Model]
parseFromFile path = do
  file <- readFile path
  case runDBParser file of
        Left e -> error ("could not parse: " ++ show e)
        Right x -> pure $ addReferenceInfo x

-- |Runs the lexer and then the parser
runDBParser :: String -> Either ParseError [Model]
runDBParser xs = runLexer xs >>= \ts -> parse parseDB "" ts

-- |The main parser
parseDB :: TokenParser [Model]
parseDB = many parseModel

-- |Parser for one model
parseModel :: TokenParser Model
parseModel = do
  satisfyT (==TModel)
  TModelName name <- satisfyT isModelName
  satisfyT (==TModelStart)
  fields' <- sepBy parseField (satisfyT (==TFieldSep))
  satisfyT (==TModelEnd)
  return $ Model name fields' []
    where isModelName (TModelName _) = True
          isModelName _              = False

-- |Parser for a field
parseField :: TokenParser Field
parseField = do
  TFieldName name <- satisfyT isFieldName
  satisfyT (==TFieldTypeSep)
  fieldType <- parsePrim <|> parseNul <|> parseBasic
               <|> parseRefSome TRefOne RefOne
               <|> parseRefSome TRefMany RefMany
  return $ Field name fieldType
    where isFieldName   (TFieldName _)   = True
          isFieldName   _                = False

-- |Parser for a ref field
parseRefSome :: Token -> (String -> FieldType) -> TokenParser FieldType
parseRefSome tt dt = satisfyT (==tt) >> satisfyT isModelRef 
               >>= \(TModelRef n) -> return $ dt n
  where isModelRef (TModelRef _) = True
        isModelRef _             = False

-- |Parser for a prim token
parsePrim :: TokenParser FieldType
parsePrim = satisfyT (==TPrimType) >> return PrimKey

-- |Parser for a nul token
parseNul :: TokenParser FieldType
parseNul = satisfyT (==TNullable) >> Nullable <$> parseBaseType

-- |Parser for base types
parseBasic :: TokenParser FieldType
parseBasic = Basic <$> parseBaseType

parseBaseType :: TokenParser BaseType
parseBaseType = parseInt <|> parseBool <|> parseString

parseInt :: TokenParser BaseType
parseInt = satisfyT (==TFieldType "Int") >> return Integer

parseBool :: TokenParser BaseType
parseBool = satisfyT (==TFieldType "Boolean") >> return Boolean

parseString :: TokenParser BaseType
parseString = satisfyT (==TFieldType "String") >> satisfyT isFieldLength 
              >>= \(TFieldLength l) -> return $ VarChar l
  where isFieldLength (TFieldLength _) = True
        isFieldLength _                = False

-- |satisfy Parser helper
satisfyT :: (Stream s m Token) => (Token -> Bool) -> ParsecT s u m Token
satisfyT p = tokenPrim showTok nextPos testTok
    where
      showTok       = show
      testTok t     = if p t then Just t else Nothing
      nextPos sp t s = case t of
        TFieldSep -> setSourceColumn (incSourceLine sp 1) 1
        TModelStart -> setSourceColumn (incSourceLine sp 1) 1
        TModelEnd   -> setSourceColumn (incSourceLine sp 3) 1
        _         -> incSourceColumn sp 1
        

