module Parser.Lexer where

import Parser.ParserTypes ( Token(..) )
import Text.Parsec
    ( Parsec(..),
      ParseError,
      alphaNum,
      char,
      spaces,
      string,
      choice,
      many1,
      optionMaybe,
      (<|>),
      many,
      parse )
import Text.ParserCombinators.Parsec.Number ( int )
import Data.Maybe ( maybeToList )

type Parser a = Parsec String () a

-- |Runs the entire lexer on a string (the real DSL) and return a list
-- of tokens.
runLexer :: String -> Either ParseError [Token]
runLexer = parse lexDB ""

-- |The lexer for the entire file
lexDB :: Parser [Token]
lexDB = spaces >> concat <$> many lexModel

-- |The lexer for one database model
lexModel :: Parser [Token] 
lexModel = endWithSpaces $ do
  modelToken <- lexModelToken
  modelName <- lexModelName
  modelStart <- lexModelStart
  fields <- lexFields
  modelEnd <- lexModelEnd
  return $ [modelToken, modelName, modelStart] ++ fields ++ [modelEnd]

-- |The lexer for the Model token that signifies a new model in our
-- DSL
lexModelToken :: Parser Token
lexModelToken = endWithSpaces $ string "Model" >> return TModel

-- |The lexer for the model name, which can be any word
lexModelName :: Parser Token
lexModelName = endWithSpaces (TModelName <$> many1 alphaNum)

-- |The lexer for the model start, "= {"
lexModelStart :: Parser Token
lexModelStart = endWithSpaces $ char '=' >> spaces >> char '{' >> return TModelStart

-- |The lexer for the model end, "}"
lexModelEnd :: Parser Token
lexModelEnd = endWithSpaces $ char '}' >> return TModelEnd

-- |The lexer for the fields
lexFields :: Parser [Token]
lexFields = endWithSpaces $ do
  f <- lexField
  fs <- lexFields'
  return $ concat (f:fs)
  where
    lexFields' = many $ do
      s <- lexFieldSep
      f <- lexField
      return (s:f)

-- |The lexer for one field
lexField :: Parser [Token]
lexField = endWithSpaces $ do
  fieldName <- lexFieldName
  typeSep <- lexFieldTypeSep
  fieldType <- lexFieldType <|> lexFieldRef
  fieldLength <- maybeToList <$> optionMaybe lexFieldLength
  return $ [fieldName, typeSep] ++ fieldType ++ fieldLength

-- |The lexer for the field seperator, ","
lexFieldSep :: Parser Token
lexFieldSep = endWithSpaces $ char ',' >> return TFieldSep

-- |The lexer for the field name, can be any word
lexFieldName :: Parser Token
lexFieldName = endWithSpaces (TFieldName <$> many1 alphaNum)

-- |The lexer for the field type
lexFieldType :: Parser [Token]
lexFieldType = endWithSpaces $ 
  ((:[]) <$> lexPrim) <|> ((++) <$> (maybeToList <$> optionMaybe lexNul) <*> ((:[])<$> lexType))

-- |The lexer for a field reference, used when the field is a
-- reference field. Can be either "ManyRef" or "OneRef" followed by a
-- word
lexFieldRef :: Parser [Token]
lexFieldRef = endWithSpaces $ do
  refTok <- lexRefMany <|> lexRefOne
  modRef <- TModelRef <$> many1 alphaNum
  return [refTok, modRef]

-- |The lexer for the field type seperator following the field name, "::"
lexFieldTypeSep :: Parser Token 
lexFieldTypeSep = endWithSpaces $ string "::" >> return TFieldTypeSep

-- |The lexer for the field length, gives a length for a string field
lexFieldLength :: Parser Token
lexFieldLength = endWithSpaces $ char '(' >> int >>= \n -> char ')' >> return (TFieldLength n)

-- |The lexer for a primary signifier for a type, "Primary"
lexPrim :: Parser Token
lexPrim = endWithSpaces $ string "Primary" >> return TPrimType

-- |The lexer for the nullable signifier for a type, "Nullable"
lexNul :: Parser Token
lexNul = endWithSpaces $ string "Nullable" >> return TNullable

-- |The lexer for the RefMany signifier, "ManyRef"
lexRefMany :: Parser Token
lexRefMany = endWithSpaces $ string "ManyRef" >> return TRefMany

-- |The lexer for the RefOne signifier, "OneRef"
lexRefOne :: Parser Token
lexRefOne = endWithSpaces $ string "OneRef" >> return TRefOne

-- |The lexer for the primitive type, can be either "Int", "String" or "Boolean"
lexType :: Parser Token
lexType = endWithSpaces (TFieldType <$> choice (map string types))

-- |A lexer that ends another lexer with whitespace. Almost all these
-- lexers end with whitespace, because we want to allow arbitrary
-- whitespace between tokens, but we shouldn't parse them at the start
-- of a lexer, because it might consume to much when it is not appropriate.
endWithSpaces :: Parser a -> Parser a
endWithSpaces p = p >>= \t -> spaces >> return t

-- |The possible primitive types
types :: [String]
types = ["Int", "String", "Boolean"]
