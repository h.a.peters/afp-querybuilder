{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE ImplicitParams #-}
{-# LANGUAGE RankNTypes #-}

module Parser.CodeGenTH (
  generateModels,
  mkModels,
  parseModels,
  mkOnUpdateMany,
  mkOnUpdateOne,
  mkOnCreateMany,
  mkOnCreateOne,
  mkIsCreateable,
  printGenCode
) where

import Data.Maybe ( mapMaybe )
import Data.Char (toLower)
import Data.Proxy ( Proxy(..) )

import GHC.TypeLits ( KnownSymbol )
import qualified GHC.Records as Record

import Database.PostgreSQL.Simple.FromRow ( FromRow )
import Database.PostgreSQL.Simple.ToRow ( ToRow )

import Language.Haskell.TH

import Parser.Parser (parseFromFile)
import Parser.ParserTypes
    ( fieldType,
      foreignKeyName,
      getPrimKey,
      BaseType(..),
      Field(..),
      FieldType(..),
      Model(fields, referencedBy, tableName) )
import Parser.Validator
import Parser.SqlGen

import Helpers
import Typeclasses
import Types
import QueryBuilder(
  createRecordUnsafe,
  updateRecord
  )

defBang :: Bang
defBang = Bang NoSourceUnpackedness NoSourceStrictness

-- | The 'generateModels' function generates haskell code
-- from models specified in a .dbhs file. It takes one
-- argument of type 'FilePath' that contains the location
-- of the .dbhs file. This function should be used in a 
-- Template Haskell splice like so:
-- @
--    $(generateModels "path/to/file.dbhs")
-- @
generateModels :: FilePath -> Q [Dec]
generateModels file = parseModels file >>= mkModels

-- | The 'mkModels' function generates all haskell code
-- needed for the parsed models from a dbhs file. It takes
-- one argument of type '[Model]' and returns a 'Q [Dec]' that
-- can be spliced in a .hs file using Template Haskell.
mkModels :: [Model] -> Q [Dec]
mkModels models = (++[mkGetDbConn models]) . concat <$> mapM mkModel models

-- Generates a function that a user can use to migrate the models to postgres.
mkGetDbConn :: [Model] -> Dec
mkGetDbConn models = FunD (mkName "mkDbConn") [ 
  Clause 
    [VarP (mkName "config")] 
    (NormalB (VarE (mkName "getDbConn") `AppE` 
              VarE (mkName "config") `AppE`
              (ConE (mkName "MigrateSQL") `AppE` LitE (StringL (generateSQL models)))))
    []
  ]

-- Helper function that prints the generated code.
-- For debug purposes.
printGenCode :: [Dec] -> Q ()
printGenCode = runIO . putStrLn . pprint

-- | The function 'parseModels' tries to parse a .dbhs file
-- and runs validation over it. It takes one argument of
-- type 'FilePath' that contains the location of the
-- .dbhs file.
parseModels :: FilePath -> Q [Model]
parseModels f = do 
  models <- runIO $ parseFromFile f
  case validateModel models of
    Just xs -> error xs
    Nothing -> return models

-- Generates the data type and all its instances for a single model.
-- The instance generation is split up in different functions.
mkModel :: Model -> Q [Dec]
mkModel model = pure $
  DataD [] modelName [] Nothing [constr] [DerivClause Nothing [ConT ''Show]]
  : mkHasPrimKey model
  : mkFromRow model
  : mkToRow model
  : mkIsQueryable model
  : mkIsUpdatable model
  : mkFieldInstances model
  ++ mkRelations model
  ++ mkIsCreateable model
  where
    modelName = mkName $ tableName model
    constr = RecC modelName recordFields
    recordFields = normalFields ++ referenceFields
    normalFields = map recordName (fields model)
    referenceFields = map (recordName' . fst) $ referencedBy model
    recordName field@(Field name t) = (mkName name, defBang, getFieldType t)
    recordName' n = (mkName $ foreignKeyName n, defBang, ConT $ mkName "Int")

-- Helper function that returns the Template Haskell type for a FieldType
-- in the Model AST.
getFieldType :: FieldType -> Type
getFieldType (Nullable bt) = undefined
getFieldType (Basic bt) = getBaseDec bt 
getFieldType PrimKey = ConT ''Int
getFieldType (RefMany mName) = AppT (ConT ''Maybe) (AppT ListT $ ConT (mkName mName))
getFieldType (RefOne mName) = AppT (ConT ''Maybe) (ConT (mkName mName))

-- Helper function that returns the Template Haskell type for a BaseType
-- in the Model AST. Used by getFieldType.
getBaseDec :: BaseType -> Type
getBaseDec Integer = ConT ''Int
getBaseDec Boolean = ConT ''Bool
getBaseDec (VarChar _) = ConT ''String

-- This function creates overloaded labels instances for each field of a model.
-- This is in the form of SetField instances of which the definition can be found in
-- Helpers.hs.
mkFieldInstances :: Model -> [Dec]
mkFieldInstances model = map realFieldsInstance (fields model)
                         ++ map (fieldInstance (ConT $ mkName "Int") . foreignKeyName . fst)
                            (referencedBy model)
  where
    realFieldsInstance (Field name t) = fieldInstance (getFieldType t) name
    fieldInstance t name  = InstanceD Nothing [] 
                                        (ConT ''SetField `AppT` LitT (StrTyLit name) `AppT`
                                         ConT (mkName $ tableName model) `AppT` t) 
                                        [mkDec name]
    mkDec name            = FunD (mkName "setField") [Clause mkParams (mkBody name) []]
    mkParams              = [ VarP $ mkName "v"
                            , VarP $ mkName "table"]
    mkBody name           = NormalB $ RecUpdE (VarE $ mkName "table")
                            [(mkName name, VarE (mkName "v"))]

-- Generates the HasPrimKey instance that facilitates getting and setting the primary key value.
mkHasPrimKey :: Model -> Dec
mkHasPrimKey model = let (Field fname _) = getPrimKey model in
  InstanceD Nothing [] (ConT ''HasPrimKey `AppT` ConT (mkName (tableName model)))
  [mkGetVal fname, mkSet fname , mkGetField fname]
  where
    mkGetVal fname   = FunD (mkName "getPrimKeyValue") 
                       [Clause [] (NormalB $ UInfixE (VarE (mkName "show"))
                                             (VarE (mkName "."))
                                             (VarE (mkName "get") `AppE` 
                                             LabelE fname)) []]
    mkSet fname      = FunD (mkName "setPrimKeyValue")
                       [Clause [] (NormalB $ UInfixE (VarE (mkName "set") `AppE`
                                             LabelE fname) 
                                             (VarE (mkName ".")) 
                                             (VarE (mkName "read"))) []]
    mkGetField fname = FunD (mkName "getPrimKeyField")
                       [Clause [] (NormalB $ LitE (StringL fname)) []]

-- Generates the FromRow instance needed for the PostGres library.
mkFromRow :: Model -> Dec
mkFromRow model = InstanceD Nothing [] 
                  (ConT ''FromRow `AppT` ConT (mkName (tableName model))) 
                  [mkDec]
  where
    mkDec = FunD (mkName "fromRow")
            [Clause [] (NormalB $ UInfixE (ConE (mkName (tableName model)))
                                          (VarE (mkName "<$>"))
                                          (mkChain f)) []]
    f = map Left (fields model) ++ map (Right . fst) (referencedBy model) 
    mkChain :: [Either Field String] -> Exp
    mkChain [Left (Field n t)] = getTypeInput t
    mkChain (Left (Field n t):fs) = UInfixE (getTypeInput t)
                                     (VarE (mkName "<*>"))
                                     (mkChain fs)
    mkChain [Right _] = VarE (mkName "field")
    mkChain (Right _:fs) = UInfixE (VarE (mkName "field"))
                                     (VarE (mkName "<*>"))
                                     (mkChain fs)
    getTypeInput t = if isRef t 
                      then VarE (mkName "pure") `AppE` ConE (mkName "Nothing")
                      else VarE (mkName "field")

    isRef = \case
              (RefMany _) -> True
              (RefOne  _) -> True
              _           -> False

-- Generates the ToRow instance needed for the PostGres library.
mkToRow :: Model -> Dec 
mkToRow model = InstanceD Nothing [] (ConT ''ToRow `AppT` 
                                      (ConT ''FunctionalMask `AppT` 
                                      ConT (mkName (tableName model)))) 
                                      [mkDec]
  where
    functionalFields = [n | (Field n t) <- fields model, needsToField t]
                       ++ map (foreignKeyName . fst) (referencedBy model)

    mkDec = FunD (mkName "toRow")
            [Clause mkParam (NormalB $ 
              ListE $ map mkToField functionalFields) []]

    mkParam = [ConP (mkName "FunctionalMask") [VarP (mkName "x")]]

    mkToField name = VarE (mkName "toField") `AppE` 
                     (VarE (mkName name) `AppE` 
                      VarE (mkName "x"))
    needsToField = \case
                      PrimKey   -> False
                      RefMany _ -> False 
                      RefOne  _ -> False 
                      _         -> True

-- Generates the Is Queryable instance
mkIsQueryable :: Model -> Dec
mkIsQueryable model = InstanceD Nothing [] (ConT ''IsQueryable `AppT` 
                                            ConT (mkName (tableName model))) 
                                            [mkNewRec, mkFuncFields]
  where
    mkNewRec = FunD (mkName "newRecord")
               [Clause [] (NormalB $ mkDefs (length (fields model)
                                             + length (referencedBy model))) []]

    mkDefs 0     = ConE (mkName (tableName model))
    mkDefs i     = mkDefs (i - 1) `AppE` VarE (mkName "def")

    functionalFields = [n | (Field n t) <- fields model, needsField t]
                       ++ map (foreignKeyName . fst) (referencedBy model)
    mkFuncFields = FunD (mkName "functionalFields")
                   [Clause [] (NormalB $ 
                    ListE $ map (LitE . StringL) functionalFields) []]
    
    needsField = \case
                      PrimKey   -> False
                      RefMany _ -> False 
                      RefOne  _ -> False 
                      _         -> True

-- Creates the relation and the foreign key
mkRelations :: Model -> [Dec]
mkRelations model = concatMap mkInstances $ fields model
  where
    mkInstances :: Field -> [Dec]
    mkInstances (Field refName (RefMany refTable))
      = [mkForeignKeyInstance refTable,
         mkManyRelationInstance refName refTable]
    mkInstances (Field refName (RefOne refTable))
      = [mkForeignKeyInstance refTable,
         mkOneRelationInstance refName refTable]
    mkInstances _           = []

    mkForeignKeyInstance table
      = InstanceD Nothing [] (ConT ''HasForeignKey `AppT`
                              ConT (mkName table) `AppT`
                              ConT (mkName $ tableName model))
        [FunD (mkName "getForeignKeyField")
         [Clause [] (NormalB $ LitE $ StringL $ foreignKeyName $ tableName model) []]
        ]

    mkManyRelationInstance refName refTable
      = InstanceD Nothing [] (ConT ''HasManyRelation `AppT`
                              ConT (mkName $ tableName model) `AppT`
                              ConT (mkName refTable)) [
        FunD (mkName "setRelatedMany") [
            let v = mkName "v"
                x = mkName "x"
            in Clause [VarP v, VarP x]
               (NormalB $ RecUpdE (VarE x) [(mkName refName, VarE v)]) []
            ],
        FunD (mkName "getRelatedMany") [
          Clause [] (
              NormalB $ AppE (VarE $ mkName "get") (LabelE refName)) []]
        ]

    mkOneRelationInstance refName refTable
      = InstanceD Nothing [] (ConT ''HasOneRelation `AppT`
                              ConT (mkName $ tableName model) `AppT`
                              ConT (mkName refTable)) [
        FunD (mkName "setRelatedOne") [
            let v = mkName "v"
                x = mkName "x"
            in Clause [VarP v, VarP x]
               (NormalB $ RecUpdE (VarE x) [(mkName refName, VarE v)]) []
            ],
        FunD (mkName "getRelatedOne") [
          Clause [] (
              NormalB $ AppE (VarE $ mkName "get") (LabelE refName)) []]
        ]

modelRelations :: Model -> [FieldType]
modelRelations model = mapMaybe (getRelation . fieldType) $ fields model
  where 
    getRelation r@(RefMany _) = Just r
    getRelation r@(RefOne _)  = Just r
    getRelation _             = Nothing

modelNames :: Model -> [String]
modelNames model = mapMaybe (getName . fieldType) $ fields model
  where 
    getName (RefMany n) = Just n
    getName (RefOne n)  = Just n
    getName _           = Nothing

mkIsUpdatable :: Model -> Dec
mkIsUpdatable model = InstanceD Nothing [] (ConT ''IsUpdatable `AppT`
                                            ConT (mkName (tableName model)))
                      [mkNewSubUpdates]
  where
    mkNewSubUpdates = FunD (mkName "subUpdates")
                      [Clause [] subUpdatesBody []]
    subUpdatesBody = NormalB $ ListE $ map createRelation $ modelRelations model
    callFunc f n = AppTypeE (VarE $ mkName f)  (ConT $ mkName n)
                   `AppE` LabelE (foreignKeyName $ tableName model)
    createRelation (RefMany n) = callFunc "mkOnUpdateMany" n
    createRelation (RefOne n) = callFunc "mkOnUpdateOne" n

-- Only creates the IsCreatable instance for models that aren't referenced and adds their references
-- as subcreates.
mkIsCreateable :: Model -> [Dec]
mkIsCreateable model
  | null $ referencedBy model = [
      InstanceD Nothing [] (ConT ''IsCreateable `AppT` ConT (mkName $ tableName model))
      [FunD (mkName "subCreates") [
           Clause [] (NormalB $ ListE $ map createRelation $ modelRelations model) []
           ]
      , FunD (mkName "subTables") [
           Clause [] (NormalB $ ListE $ map (LitE . StringL) $ modelNames model) []
           ]
      ]]
  | otherwise                 = []
  where createRelation (RefMany n) = callFunc "mkOnCreateMany" n
        createRelation (RefOne n)  = callFunc "mkOnCreateOne" n
        callFunc f n = AppTypeE (VarE $ mkName f)  (ConT $ mkName n)
                       `AppE` LabelE (foreignKeyName $ tableName model)

-- Builds the on create callback for the one cardinality case. This function is as an atom in the code generation
mkOnCreateOne :: forall related table foreignField value. 
                 (KnownSymbol foreignField, Read value, Record.HasField foreignField related value, 
                 SetField foreignField related value, IsQueryable table, IsQueryable related, HasOneRelation table related) 
                 => Proxy foreignField -> table -> DBIO table
mkOnCreateOne _ record = do 
    item <- getRelatedOne @table @related record 
                  |>> set (Proxy @foreignField) (read @value (getPrimKeyValue record))
                  |> traverse createRecordUnsafe
    record 
         |> setRelatedOne item
         |> pure

-- Builds the on create callback for the many cardinality case. This function is as an atom in the code generation
mkOnCreateMany :: forall related table foreignField value. 
                  (KnownSymbol foreignField, Read value, Record.HasField foreignField related value, 
                  SetField foreignField related value, IsQueryable table, IsQueryable related, HasManyRelation table related) 
                  => Proxy foreignField -> table -> DBIO table
mkOnCreateMany _ record = do 
    items <- getRelatedMany @table @related record 
                  |>> map (set (Proxy @foreignField) (read @value (getPrimKeyValue record)))
                  |> traverse (traverse createRecordUnsafe)
    record 
         |> setRelatedMany items
         |> pure

-- Builds the on update callback for the one cardinality case. This function is as an atom in the code generation
mkOnUpdateOne :: forall related table foreignField value. 
                  (KnownSymbol foreignField, Read value, Record.HasField foreignField related value, 
                  SetField foreignField related value, IsQueryable table,
                  IsQueryable related, HasOneRelation table related, IsUpdatable related) 
                  => Proxy foreignField -> table -> DBIO ()
mkOnUpdateOne _ record = getRelatedOne @table @related record
                         |>> set (Proxy @foreignField) (read @value (getPrimKeyValue record))
                         |> traverse updateRecord >> pure ()
        
-- Builds the on update callback for the many cardinality case. This function is as an atom in the code generation
mkOnUpdateMany :: forall related table foreignField value. 
                  (KnownSymbol foreignField, Read value, Record.HasField foreignField related value, 
                  SetField foreignField related value, IsQueryable table,
                  IsQueryable related, HasManyRelation table related, IsUpdatable related) 
                  => Proxy foreignField -> table -> DBIO ()
mkOnUpdateMany _ record = getRelatedMany @table @related record
                          |>> map (set (Proxy @foreignField) (read @value (getPrimKeyValue record)))
                          |> traverse (traverse updateRecord) >> pure ()
