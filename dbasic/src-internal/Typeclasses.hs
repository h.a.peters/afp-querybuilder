{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE IncoherentInstances #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE FlexibleContexts#-}
{-# LANGUAGE ImplicitParams #-}

module Typeclasses where

import Helpers

import Data.Typeable ( Typeable )
import qualified Data.ByteString.Lazy.Char8 as BS

import Database.PostgreSQL.Simple.FromRow ( FromRow )
import Database.PostgreSQL.Simple.FromField ( FromField(..) )
import Database.PostgreSQL.Simple.ToRow ( ToRow )
import Types

newtype Key = Key String

instance FromField Key where
    fromField _ (Just b) = (pure . Key . BS.unpack . BS.fromStrict) b

-- | This typeclass is an aggregation of other typeclasses needed
-- Typeable allows to convert a type to it's string representation
-- FromRow allows for use with PostgresSQL
class 
    ( Typeable table
    , FromRow table
    , ToRow (FunctionalMask table)
    , HasPrimKey table
    , Show table
    ) => IsQueryable table where
    newRecord :: table
    functionalFields :: [String]

-- | The 'IsCreateable' typeclass is used for all models
-- to implement that they are createable and can create new rows in
-- the database.
class (IsQueryable table) => IsCreateable table where
    -- Is called to create instances for a child table
    subCreates :: (?conn :: DbConn) => [table -> IO table]
    -- Gets all names of the children tables.
    subTables :: [String]
    -- Is called to create a record in the table itself.
    onCreate :: (?conn :: DbConn) => table -> IO table
    onCreate parent = sequenceIO parent subCreates

-- | The 'IsUpdatable' typeclass is used for all models
-- to implement that they are updateable and can thus update rows in
-- the database.
class (IsQueryable table) => IsUpdatable table where
    -- is called to update instances of related child instances.
    -- empty as a default.
    subUpdates :: (?conn :: DbConn) => [table -> IO ()]
    subUpdates = []
    -- is called to update a record. This may update child instances as well.
    onUpdate :: (?conn :: DbConn) => table -> IO ()
    onUpdate parent = mapM_ ($parent) subUpdates

-- | Contains all fields that are not the primary key.
-- Used during creation, updating and deletion of rows.
newtype FunctionalMask a = FunctionalMask a

-- | The 'HasPrimKey' typeclass allows us to easily get/set
-- the primkey value / field.
class HasPrimKey table where
    getPrimKeyValue :: table -> String
    setPrimKeyValue :: String -> table -> table
    getPrimKeyField :: String

-- | The 'HasForeignKey' typeclass allows us to obtain the
-- field of a foreign key. The child model has an instance of this class.
class HasForeignKey table relation where
    getForeignKeyField :: String

-- | The 'HasManyRelation' typeclass allows the parent table to set and get
-- the related table for a one to many relation.
class (IsQueryable table, IsQueryable related, HasForeignKey related table) 
      => HasManyRelation table related where
    setRelatedMany :: Maybe [related] -> table -> table
    getRelatedMany :: table -> Maybe [related]

-- | The 'HasOneRelation' typeclass allows the parent table to set and get
-- the related table for a one to one relation.
class (IsQueryable table, IsQueryable related, HasForeignKey related table) 
      => HasOneRelation table related where
    setRelatedOne :: Maybe related -> table -> table
    getRelatedOne :: table -> Maybe related

-- | The ShowSQL instance is used to show a value in a
-- SQL syntax.
class ShowSQL a where
    showSQL :: a -> String

-- | The general instance in which the double quotes are replaced by
-- single quotes.
instance (Show val) => ShowSQL val where
  showSQL xs = map replace (show xs)
    where replace '\"' = '\''
          replace c    = c
