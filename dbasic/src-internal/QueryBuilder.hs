{-# LANGUAGE GADTs #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE ImplicitParams #-}

module QueryBuilder where

import GHC.TypeLits ( KnownSymbol, symbolVal )
import GHC.Records as Record ( HasField )

import Control.Monad

import Data.Proxy ( Proxy )
import Data.Typeable ( Typeable, Proxy )
import Data.List
import Data.Maybe
import qualified Data.Text as T
--import qualified Data.ByteString.Lazy.Char8 as BS
import qualified Data.ByteString.UTF8 as BSU

import qualified Database.PostgreSQL.Simple as PG
import Database.PostgreSQL.Simple.ToRow ( toRow )
import Database.PostgreSQL.Simple.Types

import Parser.ParserTypes (foreignKeyName)
import Helpers
import Types
import Typeclasses

-- | 'Operator' defines different basic where clause restrictions on different fields.
data Operator = Eq | Ne | Gt | Lt

instance Show Operator where
    show Eq = "="
    show Ne = "<>"
    show Gt = ">"
    show Lt = "<"

-- | 'Condition' defines And and Or relations between different operators.
data Condition = And Condition Condition
               | Or Condition Condition
               | Var String
               deriving (Show, Eq)

-- builds an SQL string for And and Or conditions.
compileCondition :: Condition -> String
compileCondition (Var x) = x
compileCondition (And x y) = "(" <> compileCondition x <> " AND " <> compileCondition y <> ")"
compileCondition (Or x y) = "(" <> compileCondition x <> " OR " <> compileCondition y <> ")"

-- Record that represents all parts of a query, is built by the buildQuery function.
data SQLQuery = SQLQuery
  { selectTable :: String
  , filterConds :: Maybe Condition
  , limitCond :: Maybe Int
  , skipCond :: Maybe Int
  } deriving (Show)

instance SetField "selectTable" SQLQuery String where
  setField v SQLQuery {..} = let selectTable = v in SQLQuery {..}
instance SetField "filterConds" SQLQuery (Maybe Condition) where
  setField v SQLQuery {..} = let filterConds = v in SQLQuery {..}
instance SetField "limitCond" SQLQuery (Maybe Int) where
  setField v SQLQuery {..} = let limitCond = v in SQLQuery {..}
instance SetField "skipCond" SQLQuery (Maybe Int) where
  setField v SQLQuery {..} = let skipCond = v in SQLQuery {..}

data QueryBuilder table where
    MemptyBuilder :: IsQueryable table => QueryBuilder table
    FilterBuilder :: (IsQueryable table, ShowSQL value) => String -> Operator -> value -> QueryBuilder table -> QueryBuilder table
    OrBuilder     :: (IsQueryable table) => [QueryBuilder table] -> QueryBuilder table -> QueryBuilder table
    AndBuilder    :: (IsQueryable table) => [QueryBuilder table] -> QueryBuilder table -> QueryBuilder table
    LimitBuilder :: (IsQueryable table) => Int -> QueryBuilder table -> QueryBuilder table
    SkipBuilder :: (IsQueryable table) => Int -> QueryBuilder table -> QueryBuilder table

-- | The function 'buildQuery' translates a Haskell code level query and translates it to the
-- 'SQLQuery' datatype. It takes a 'QueryBuilder' as argument
-- You should not need to use this function as it is automatically called when one of the fetch functions is called.
buildQuery :: forall table. (Typeable table, IsQueryable table) => QueryBuilder table -> SQLQuery
-- In the case that we find an 'MemptyBuilder, only the select table part of the SQLQuery is filled in.
buildQuery MemptyBuilder = SQLQuery 
        { selectTable = typeAsString @table
        , filterConds = Nothing
        , limitCond = Nothing
        , skipCond = Nothing
        }

-- When we encounter a FilterBuilder, we first run 'qb' which represents the rest of the query in the builder.
-- Then, we build up the condition which is then added to the filterConds field of SQLQuery.
buildQuery (FilterBuilder field op val qb) = let
    prevQ = buildQuery qb
    cond = Var $ field <> showSQL op <> showSQL val
    in prevQ 
        |> modify #filterConds (\conds -> Just cond <&?&> conds)

buildQuery (OrBuilder []    oldQ) = buildQuery oldQ
-- When we encounter an OrBuilder with or subqueries, we again first build the rest of the query.
-- Afterwards, the 'subQs' are folded together using the 'orFold' Function. The result is added
-- to the filterConds.
buildQuery (OrBuilder subQs oldQ) = let
    prevQ = buildQuery oldQ
    orCond = orFold $ map (get #filterConds . buildQuery) subQs
    in prevQ
        |> modify #filterConds (orCond <&?&>)

buildQuery (AndBuilder []    oldQ) = buildQuery oldQ
-- Similar to the OrBuilder but uses the andFold function to apply all 'subQs' with Ands.
buildQuery (AndBuilder subQs oldQ) = let
    prevQ = buildQuery oldQ
    andCond = andFold $ map (get #filterConds . buildQuery) subQs
    in prevQ
        |> modify #filterConds (andCond <&?&>)

-- The LimitBuilder takes an 'Int' that is used in the LIMIT clause in the query.
buildQuery (LimitBuilder n oldQ) = buildQuery oldQ
                                    |> set #limitCond (Just n)
-- The SkipBuilder takes an 'Int' that is used in the OFFSET clause in the query.
buildQuery (SkipBuilder n oldQ) = buildQuery oldQ
                                    |> set #skipCond (Just n)

-- | support function for 'buildQuery'. 'andFold' takes a '[Maybe Condition]'
-- and folds them using the 'And' condition.
andFold :: [Maybe Condition] -> Maybe Condition
andFold [] = Nothing 
andFold (Just x:xs) = Just (x <&&> andFold xs)
andFold (Nothing:xs)  = andFold xs

-- | support function for 'buildQuery'. 'orFold' takes a '[Maybe Condition]'
-- and folds them using the 'Or' condition.
orFold :: [Maybe Condition] -> Maybe Condition
orFold [] = Nothing 
orFold (Just x:xs) = Just (x <||> orFold xs)
orFold (Nothing:xs)  = orFold xs

-- Adds a 'Condition' and a 'Maybe Condition' together using the 'And' condition.
-- Slightly abusing the Maybe monad by circumventing the fact the a Nothing should
-- halt a map.
(<&&>) ::  Condition -> Maybe Condition -> Condition
(<&&>) x Nothing  = x
(<&&>) y (Just x) = And x y

-- Add two 'Maybe Condition' together using the 'And' condition.
(<&?&>) ::  Maybe Condition -> Maybe Condition -> Maybe Condition
(<&?&>) x Nothing = x
(<&?&>) Nothing y = y
(<&?&>) x y = And <$> x <*> y

-- Adds a 'Condition' and a 'Maybe Condition' together using the 'Or' condition.
(<||>) :: Condition -> Maybe Condition -> Condition
(<||>) x Nothing  = x
(<||>) y (Just x) = Or x y

-- 'toSQL' converts an 'SQLQuery' to its corresponding SQL 'String'.
toSQL :: SQLQuery -> String
toSQL SQLQuery {..} = "SELECT * FROM "   <>  selectTable <> " "
                   <> maybe "" ("WHERE " <>) (compileCondition <$> filterConds) <> " "
                   <> maybe "" ("LIMIT " <>) (show <$> limitCond) <> " "
                   <> maybe "" ("OFFSET "  <>) (show <$> skipCond) <> " "
                   <> ";"

-- | The function 'query' builds, given a table, a MemptyBuilder for the table.
-- Usage is as follows:
-- @
--    query @Blog
-- @
query :: forall table. IsQueryable table => QueryBuilder table
query = MemptyBuilder @table

-- | The function 'queryOr' takes a list of 'whereClause' applications
-- This is denoted as a list of functions because the whereClauses are not provided
-- with the followup 'QueryBuilder' argument as it is not needed (the OrBuilder takes care of that).
-- Usage is as follows:
-- @
--    queryOr [ whereClause #author Eq "Hugo",
--              whereClause #author Eq "Niels"
--              ]
-- @
queryOr :: forall table. (IsQueryable table)
           => [QueryBuilder table -> QueryBuilder table]
           -> QueryBuilder table -> QueryBuilder table
queryOr fxs = OrBuilder (map ($ MemptyBuilder) fxs)

-- | The function 'queryAnd' is similar to the 'queryOr' function but is different
-- in the sense that it builds an 'AndBuilder' which is stronger in the sense of priority.
-- It is only needed for nested Ors and Ands. chained where clauses are automatically folded using 'And'.
queryAnd :: forall table. (IsQueryable table)
            => [QueryBuilder table -> QueryBuilder table]
            -> QueryBuilder table -> QueryBuilder table
queryAnd fxs = AndBuilder (map ($ MemptyBuilder) fxs)

-- | The function 'whereClause' builds a clause that is situated in the WHERE part of a SQL query.
-- It takes a proxied field name, an 'Operator' and a value that must be of the type that the field has.
whereClause :: forall table field value. (KnownSymbol field, IsQueryable table, ShowSQL value, Record.HasField field table value) => 
               Proxy field -> Operator -> value -> QueryBuilder table -> QueryBuilder table
whereClause field = whereClauseUnsafe (symbolVal field)

-- whereClauseUnsafe is called when the value and the given field are type checked.
whereClauseUnsafe :: forall table value. (IsQueryable table, ShowSQL value) => 
                     String -> Operator -> value -> QueryBuilder table -> QueryBuilder table
whereClauseUnsafe = FilterBuilder

-- | 'limit' creates a LIMIT clause using the provided 'Int'
limit :: forall table. (IsQueryable table) => Int -> QueryBuilder table -> QueryBuilder table 
limit = LimitBuilder

-- | 'skip' creates a OFFSET clause using the provided 'Int'
skip :: forall table. (IsQueryable table) => Int -> QueryBuilder table -> QueryBuilder table 
skip = SkipBuilder

-- | UNSAFE: 'createRecordUnsafe' is used in 'createRecord' which is the only function you
-- should be calling. This function takes care of building SQL to create a record after type safety checks.
-- It takes a Queryable table as argument and returns the record when it is created.
createRecordUnsafe :: forall table. (IsQueryable table) => table -> DBIO table
createRecordUnsafe record = do
    let (DbConn connection) = ?conn 
    let header = "(" <> intercalate ", " (functionalFields @table) <> ")"
        values  = "(" <> intercalate ", " (replicate (length (functionalFields @table)) "?") <> ")"
    let q = Query $ BSU.fromString $ "INSERT INTO " <> typeAsString @table <> " " 
                  <> header
                  <> " VALUES " <>  values <> " RETURNING " <> getPrimKeyField @table
    [Only (Key id)] :: [Only Key] <- PG.query connection q (FunctionalMask record)
    record
        |> setPrimKeyValue id
        |> pure

-- | 'createRecord' builds the SQL for a creating a record in the database for a certain table.
-- The table must be an instance of 'IsCreatable' which enforces that only a model can be provided
-- to this function.
createRecord :: forall table. (IsCreateable table) => table -> DBIO table
createRecord record = do
    ret <- createRecordUnsafe record
    onCreate ret

_recordExistsQuery :: forall table. (IsQueryable table) => table -> String
_recordExistsQuery record = "SELECT " <> getPrimKeyField @table <> " FROM "
                            <> typeAsString @table <> " WHERE " <> getPrimKeyField @table
                            <> " = " <> getPrimKeyValue record <> ";"

-- | Checks if a record exists, based on the primary key. The function takes a table
-- that it should check.
recordExists :: forall table. (?conn :: DbConn, IsQueryable table) => table -> IO Bool
recordExists record = do
  let (DbConn connection) = ?conn
  let q = Query $ BSU.fromString $ _recordExistsQuery record
  ids :: [Only Key] <- PG.query_ connection q
  return $ not $ null ids

_updateExistingQuery :: forall table. (IsQueryable table) => table -> String
_updateExistingQuery record =
  let updates = intercalate "," $ map (<> " = ?") (functionalFields @table)
  in "UPDATE " <> typeAsString @table 
     <> " SET "   <> updates 
     <> " WHERE " <> getPrimKeyField @table <>" = " <> getPrimKeyValue record

-- |Updates the record (or creates it if it doesn't exist)
--
-- Or creates it if it doesn't exist yet. It will also update or
-- create all records that this record references (so those that are contained in
-- the record.
updateRecord :: forall table. (IsUpdatable table) => table -> DBIO table
updateRecord record = do
    let (DbConn connection) = ?conn 

    let updateExisting record = do
          -- It exists, so we simply update it
          let q = Query $ BSU.fromString $ _updateExistingQuery record
          PG.execute connection q (FunctionalMask record)
          return ()
      
    -- First we check if it already exists
    exists <- recordExists record

    if exists
      then updateExisting record
      else void (createRecordUnsafe record)

    -- Regardless, we need to update or create the references  
    onUpdate record >> pure record

-- | calls 'updateRecord' on all tables in its argument '[table]'
updateRecordAll :: forall table. (IsUpdatable table) => [table] -> DBIO [table]
updateRecordAll = mapM updateRecord

-- | The function 'fetchManyRelated' fetches a one to many relation for an entry in 'table'.
fetchManyRelated :: forall relation table. (HasManyRelation table relation) => table -> DBIO table
fetchManyRelated record = do
            items <- query @relation
                 |> whereClauseUnsafe (getForeignKeyField @relation @table) Eq (getPrimKeyValue record)
                 |> fetchAll
            pure $ setRelatedMany (Just items) record

-- | The function 'fetchManyRelatedMaybe' encapsulates 'fetchManyRelated' in a Maybe that returns 'Nothing' when
-- there are no relations.
fetchManyRelatedMaybe :: forall relation table. (HasManyRelation table relation) => Maybe table -> DBIO (Maybe table)
fetchManyRelatedMaybe = traverse (fetchManyRelated @relation)

-- | The function 'fetchManyRelatedAll' fetches all one to many relations for every 'table' in the input list.
fetchManyRelatedAll :: forall relation table. (HasManyRelation table relation) => [table] -> DBIO [table]
fetchManyRelatedAll = mapM (fetchManyRelated @relation)

-- | The function 'fetchOneRelated' fetches a one to one relation for a certain record in 'table'
-- The relation is queried on the entry that has an equal foreignkey to the primkey of the record.
fetchOneRelated :: forall relation table. (HasOneRelation table relation) => table -> DBIO table
fetchOneRelated record = do
            item <- query @relation
                 |> whereClauseUnsafe (getPrimKeyField @relation) Eq (getPrimKeyValue record)
                 |> fetchOneOrNothing
            pure $ setRelatedOne item record

-- | The function 'fetchOneRelatedMaybe' encapsulates 'fetchOneRelated' in a Maybe that returns 'Nothing' when
-- there are no relations.
fetchOneRelatedMaybe :: forall relation table. (HasOneRelation table relation) => Maybe table -> DBIO (Maybe table)
fetchOneRelatedMaybe = traverse (fetchOneRelated @relation)

-- | The function 'fetchOneRelatedAll' fetches all one to one relations for every 'table' in the input list.
fetchOneRelatedAll :: forall relation table. (HasOneRelation table relation) => [table] -> DBIO [table]
fetchOneRelatedAll = mapM (fetchOneRelated @relation)

            
-- | The function 'fetchOneOrNothing' queries the first entry of a 'table' if it has entries,
-- otherwise, it returns Nothing.
fetchOneOrNothing :: forall table. (IsQueryable table) => QueryBuilder table -> DBIO (Maybe table)
fetchOneOrNothing q = q
    |> limit 1 
    |> fetchAll
    |>> listToMaybe

-- | The function 'fetchAll' is the main query function that gets an argument of type
-- 'QueryBuilder' and converts this to sql. This sql is then queried on the database.
-- The result is returned as a list of rows.
fetchAll :: forall table. (IsQueryable table) => QueryBuilder table -> DBIO [table]
fetchAll q = queryRawSQL @table (toSQL (buildQuery q))

-- The non type safe function to delete records from the database.
_deleteRecordQueries :: forall table. (IsCreateable table) => table -> [String]
_deleteRecordQueries record = deleteSubs <> pure deleteMain
  where deleteSubs = ["DELETE FROM " <> table <>
                      " WHERE " <> foreignKeyName (typeAsString @table) <>
                      " = " <> getPrimKeyValue record
                     | table <- subTables @table]
        deleteMain = "DELETE FROM " <> typeAsString @table <>
                     " WHERE " <> getPrimKeyField @table <>
                     " = " <> getPrimKeyValue record

-- | 'deleteRecord' calls '_deleteRecordQueries' in a setting where a database connection is present
-- and executes the deletion on the database.
deleteRecord :: forall table. (IsCreateable table) => table -> DBIO ()
deleteRecord record = mapM_ runRawSQL (_deleteRecordQueries record)

-- | 'queryRawSQL' takes a raw SQL string as type 'String' and queries
-- this on the database. It returns the resulting rows.
-- The function lives the DBIO monad, which requires a DbConn to be present
-- through implicit parameters.
queryRawSQL :: (IsQueryable table) => String -> DBIO [table]
queryRawSQL q = do
  let (DbConn conn) = ?conn
  PG.query_ conn $ Query (BSU.fromString q)

-- | 'runRawSQL' takes an SQL 'String' and executes it on the database.
-- this function is used when no result is expected.
runRawSQL :: String -> DBIO ()
runRawSQL q = do
  let (DbConn conn) = ?conn
  _ <- PG.execute_ conn $ Query (BSU.fromString q)
  pure ()

-- | 'getDbConn' provides a connection to the database using a certain 'Config'.
-- The function then makes sure all tables exists in the connected database.
-- The function returns a connection that can be used to perform queries or execute
-- other database actions.
getDbConn :: Config -> MigrateSQL -> IO DbConn
getDbConn config (MigrateSQL migrateSql) = do
    conn <- DbConn <$> PG.connectPostgreSQL (getConfigStr config)
    let ?conn = conn in runRawSQL migrateSql
    pure conn

-- | 'runDB' takes a 'DbConn' and a fetchable query and executes it on the database.
runDB :: DbConn -> DBIO a -> IO a
runDB conn next = let ?conn = conn in next

  --result <- PG.execute_ connection "create table lol (prop1 SERIAL PRIMARY KEY, prop2 VARCHAR (50) NOT NULL, prop3 integer);"
  --result <- PG.execute_ connection "create table blog (blog_id SERIAL PRIMARY KEY, text VARCHAR (50), __lol_id integer, CONSTRAINT fk_lol FOREIGN KEY(__lol_id) REFERENCES lol(prop1));"
  --result <- PG.execute_ connection "insert into lol (prop1, prop2, prop3) values (20, 'Yeet', 42);"
