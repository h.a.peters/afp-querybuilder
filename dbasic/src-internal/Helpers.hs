{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE IncoherentInstances #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE FlexibleContexts#-}

module Helpers where

import GHC.OverloadedLabels (IsLabel(..))
import GHC.Records as Record ( HasField(..) )
import GHC.TypeLits ( KnownSymbol, Symbol )
import Data.Proxy ( Proxy(..) )
import Data.Typeable ( Typeable, Proxy(..), typeOf, typeRep )

-- | Sequence operator, read as 'then'
infixl 8 |>
a |> f = f a
{-# INLINE (|>) #-}

-- | Sequence operator over a functor, read as 'then if you can'
-- It's the same as fmap but a different infix to easily chain
infixl 8 |>>
a |>> f = f <$> a
{-# INLINE (|>>) #-}

-- | Sequence monadic operator over a function, read as 'then if you can and combine'
-- it's the same as bind but has a differnt infix to easily chain
-- you could also write this as  a |> (>>= f)
infixl 8 |>=
--a |>= f = (>>= f) a
a |>= f = a >>= f
{-# INLINE (|>=) #-}

class SetField (field :: GHC.TypeLits.Symbol) model value | field model -> value where
  setField :: value -> model -> model

instance forall name name'. (KnownSymbol name, name' ~ name) => IsLabel name (Proxy name') where
  fromLabel = Proxy @name'
  
-- | get a field from a record using the overloaded labels syntax
get :: forall model name value. (KnownSymbol name, Record.HasField name model value) => Proxy name -> model -> value
get _ = Record.getField @name
{-# INLINE get #-}

-- | set a field from a record using the overloaded labels syntax
set :: forall model name value. (KnownSymbol name, SetField name model value) => Proxy name -> value -> model -> model
set _ = setField @name
{-# INLINE set #-}

-- | apply the set operation on a record in a monad
setM :: forall model name value m. (Monad m, KnownSymbol name, SetField name model (m value)) 
        => Proxy name -> value -> model -> model
setM _ v = set (Proxy @name) (pure v)
{-# INLINE setM #-}

-- | modify the value of record's field using a function.
modify :: forall model name value. (KnownSymbol name, Record.HasField name model value, SetField name model value) 
          => Proxy name -> (value -> value) -> model -> model
modify x f record = record |> set x (f (get x record))
{-# INLINE modify #-}

-- | apply the modification operation on a record in a monad
modifyM :: forall model name value m. (Monad m, KnownSymbol name, 
           Record.HasField name model (m value), SetField name model (m value)) 
           => Proxy name -> (value -> value) -> model -> model
modifyM x f r = let nv = f <$> get x r in set x nv r
{-# INLINE modifyM #-}

-- | convert a type to it's string representation
typeAsString :: forall t. (Typeable t) => String
typeAsString = show $ typeRep (Proxy :: Proxy t)

isString :: (Typeable a) => a -> Bool
isString xs = typeOf xs == typeOf ""

class Arity f where
    arity :: f -> Int

instance Arity unit where
    arity _ = 0

instance Arity f => Arity ((->) a f) where
    arity f = 1 + arity (f undefined)

sequenceIO :: a -> [a -> IO a] -> IO a
sequenceIO v [] = pure v
sequenceIO v (x:xs) = x v >>= \v -> sequenceIO v xs

sequenceIO_ :: [IO ()] -> IO ()
sequenceIO_  = foldr (>>) (pure ())
