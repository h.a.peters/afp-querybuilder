# Ensure we are in app dir
if [ ! -f dbasic.cabal ]; then
    echo "You have to be in the project directory to run the script"
    exit 1
fi

GHCI_SCRIPT="$(mktemp)"
trap "rm $GHCI_SCRIPT" EXIT

echo "import Parser.CodeGenTH" >> "$GHCI_SCRIPT"
echo "import Types" >> "$GHCI_SCRIPT"
echo "import Language.Haskell.TH" >> "$GHCI_SCRIPT"
echo "let fp = \"../afp-testproj/models.dbhs\"" >> "$GHCI_SCRIPT"
echo "runQ $ parseModels fp >>= mkModels >>= printGenCode" >> "$GHCI_SCRIPT"
echo ":quit" >> "$GHCI_SCRIPT"

cabal repl < "$GHCI_SCRIPT"
