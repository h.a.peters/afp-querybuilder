module TestToSql where

import Test.Tasty
import Test.Tasty.QuickCheck as Q

import Database.DBasic.Queries

tests = testGroup "Test the toSql function"
        [ testTableName
        ]

testTableName = testProperty "Test that the table name is correct" tableNameCorrect

startsWith :: String -> String -> Bool
startsWith [] ys = True
startsWith xs [] = False
startsWith (x:xs) (y:ys) = (x == y) && startsWith xs ys

tableNameCorrect :: String -> Bool
tableNameCorrect name =
  let query = SQLQuery name Nothing Nothing Nothing
      expected = "SELECT * FROM " ++ name
  in startsWith expected (toSQL query)
