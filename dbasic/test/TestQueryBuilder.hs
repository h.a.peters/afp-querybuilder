{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
module TestQueryBuilder where

import Test.Tasty
import Test.Tasty.QuickCheck as Q
import Test.Tasty.HUnit as U

import GHC.Records as Record
import Data.List
import Data.Default
import Database.PostgreSQL.Simple.FromRow
import Database.PostgreSQL.Simple.ToRow
import Database.PostgreSQL.Simple.ToField

import Database.DBasic.Queries
import TestModels

tests :: TestTree
tests = testGroup "Test the QueryBuilder"
        [ testGeneralSelect
        , testSimpleWhere
        , testWhereAnd
        , testWhereOr
        , testWhereNested
        , testRecordExists
        , testRecordUpdate
        , testRecordDelete
        ]

testGeneralSelect = testCase "Test the general select statement" $ do
  let sqlQuery = buildQuery $ query @TestType
  selectTable sqlQuery @?= "TestType"
  filterConds sqlQuery @?= Nothing

testSimpleWhere = testCase "Test select with simple where" $ do
  let sqlQuery = buildQuery $ query @TestType
                 |> whereClause #testProp1 Lt 0
  selectTable sqlQuery @?= "TestType"
  filterConds sqlQuery @?= Just (Var "testProp1<0")

testWhereAnd = testCase "Test select with composed and where" $ do
  let conds = filterConds $ buildQuery $ query @TestType
              |> whereClause #testProp2 Ne "Hallo"
              |> whereClause #testProp1 Gt 10
  conds @?= Just (And (Var "testProp1>10") (Var "testProp2<>'Hallo'"))

testWhereOr = testCase "Test select with composed or where" $ do
  let conds = filterConds $ buildQuery $ query @TestType
              |> queryOr [
                  whereClause #testProp2 Eq "Hey"
                , whereClause #testProp1 Ne 2
                ]
  conds @?= Just (Or (Var "testProp1<>2") (Var "testProp2='Hey'"))
  
testWhereNested = testCase "Test select with nested and in or" $ do
  let conds = filterConds $ buildQuery $ query @TestType
              |> queryOr [
                  whereClause #testProp2 Eq "Hey"
                , queryAnd [whereClause #testProp1 Lt 10
                           , whereClause #testProp1 Gt 8]
                ]
  conds @?= Just (Or (And (Var "testProp1>8") (Var "testProp1<10")) (Var "testProp2='Hey'"))

testRecordExists = testCase "Test that record exists creates the right query" $ do
  let query = _recordExistsQuery $ TestType 9 1 "2" $ Just []
  query @?= "SELECT testId FROM TestType WHERE testId = 9;"

testRecordUpdate = testCase "Test that record updates creates the right query" $ do
  let query = _updateExistingQuery $ TestType 9 1 "2" $ Just []
  query @?= "UPDATE TestType SET testProp1 = ?,testProp2 = ? WHERE testId = 9"

testRecordDelete = testCase "Test that record delete creates the right queries" $ do
  let queries = _deleteRecordQueries $ TestType 9 1 "2"
                $ Just [SubTestType 0 "a" 9, SubTestType 1 "b" 9]
  length queries @?= 2
  queries @?= ["DELETE FROM SubTestType WHERE __testtype_id = 9",
               "DELETE FROM TestType WHERE testId = 9"]
