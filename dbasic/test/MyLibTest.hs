module Main (main) where

import Test.Tasty

import qualified TestQueryBuilder as QB
import qualified TestToSql as TS
import qualified TestLexer as TL

main :: IO ()
main = defaultMain tests

tests = testGroup "Tests"
        [ QB.tests
        , TS.tests
        , TL.tests
        ]
