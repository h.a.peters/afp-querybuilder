{-# LANGUAGE DataKinds #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
module TestLexer where

import Test.Tasty
import Test.Tasty.HUnit

import Parser.Lexer
import Parser.ParserTypes as T

import Text.Parsec(parse)

tests :: TestTree
tests = testGroup "Test the lexer"
        [
          testLexType
        ]

expectParse :: (Eq a, Show a) => Parser a -> String -> a -> Assertion
expectParse p s expected = case parse p "" s of
  Left e       -> assertBool (show e) False
  Right actual -> expected @=? actual

testLexType = testCase "Test the lextType function" $ do
  expectParse lexType "Int   " $ T.TFieldType "Int"
  expectParse lexType "String" $ T.TFieldType "String"
  expectParse lexType "Boolean " $ T.TFieldType "Boolean"
  
