{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE RankNTypes #-}
module Main where

import Database.DBasic.Queries
import Models

config :: Config
config = Config {
      port = 5432
    , dbName = "test_db"
    , dbUser = "postgres"
    , dbPassword = "changeme"
    , dbHost = "localhost"
  }

connection :: IO DbConn
connection = mkDbConn config

main = connection >>= \conn -> runDB conn createBlog
                  >> runDB conn getComments

viewBlogs = connection >>= \conn -> runDB conn getAllBlogs

deleteMarien = connection >>= \conn -> runDB conn deleteBlog

-- tryUpdate :: IO [Lol]
-- tryUpdate = connection >>= \conn -> runDB conn createDependModelsFromUpdate

-- leQuery :: DBIO a -> IO a
-- leQuery query = connection >>= \conn -> runDB conn query

-- q4 :: DBIO Lol
-- q4 = newRecord @Lol
--         |> setM #blogs [newRecord @Blog]
--         |> createRecord

-- joer :: DBIO Lol
-- joer = q4 
--        |>> modifyM #blogs (map (set #text "Joe"))
--        |>> modifyM #blogs (newRecord @Blog :)
--        |>= updateRecord
          


-- setAllJoe :: DBIO [Lol]
-- setAllJoe = do
--   lols <-  query @Lol
--       |> fetchAll
--       |>= fetchManyRelatedAll @Blog

--   lols
--      |>> modifyM #blogs (map (set #text "Allemaal anders"))
--      |> updateRecordAll

-- q6 :: DBIO ()
-- q6 = do
--   rs <- fetchAll (query @Lol)
--   let rs' = map (set #prop2 "mass updated!") rs
--   mapM_ updateRecord rs'


-- q8 :: Int -> DBIO [Lol]
-- q8 n = query @Lol
--         |> limit 2
--         |> skip n
--         |> fetchAll

-- q9 :: DBIO (Maybe Lol)
-- q9 = query @Lol
--         |> fetchOneOrNothing
--         |>= fetchManyRelatedMaybe @Blog

createBlog :: DBIO Blog
createBlog = newRecord @Blog
              |> set #author "Marien"
              |> setM #comments [
                  newRecord
                    |> set #writer "Hugo"
                    |> set #content "Joert"
                ]
              |> createRecord 

getComments :: DBIO [Comment]
getComments = do
  blogs <- query @Blog
            |> whereClause #author Eq "Marien"
            |> fetchAll
  concat <$> traverse (\b -> query @Comment
                        |> whereClause #writer Eq "Hugo"
                        |> whereClause #__blog_id Eq (blogId b)
                        |> fetchAll)
                        blogs

deleteBlog :: DBIO ()
deleteBlog = do
  blogs <- query @Blog
            |> whereClause #author Eq "Marien"
            |> fetchAll
  mapM_ deleteRecord blogs

getAllBlogs :: DBIO [Blog]
getAllBlogs = query @Blog
                |> fetchAll

getHugoBlogs :: DBIO [Blog]
getHugoBlogs = query @Blog
                |> whereClause #author Eq "Hugo"

-- createDependModelsFromUpdate :: DBIO [Lol]
-- createDependModelsFromUpdate = do
--   record <- q4
--   print $ get #prop1 record
--   record |> modifyM #blogs ((++) [newRecord @Blog]) |> updateRecord
--   query @Lol |> fetchAll |>= fetchManyRelatedAll @Blog

-- q5 :: IO ()
-- q5 = do
--   r <- query @Lol
--         |> fetch
--   r |> set #prop2 "updated!"
--     |> updateRecord

-- q6 :: IO ()
-- q6 = do
--   rs <- fetchAll (query @Lol)
--   let rs' = map (set #prop2 "mass updated!") rs
--   mapM_ updateRecord rs'


-- q8 :: Int -> IO [Lol]
-- q8 n = query @Lol
--         |> limit 2
--         |> skip n
--         |> fetchAll
